import XCTest

import SwiftCIDTests

var tests = [XCTestCaseEntry]()
tests += SwiftCIDTests.allTests()
XCTMain(tests)
