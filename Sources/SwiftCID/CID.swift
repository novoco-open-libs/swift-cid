import Multibase
import SwiftMultihash

public struct CID {
    private let multibase: Multibase
    private let version: Multicodec
    private let multicodec: Multicodec
    private let multihash: Multihash
    
    public init() throws {
        self.multibase = Multibase.Base58BTC
        self.version = Multicodec.cidv1
        self.multicodec = Multicodec.raw
        self.multihash = try sum([0], 0x13, -1)
    }
}
