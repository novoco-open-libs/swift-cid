//
//  File.swift
//  
//
//  Created by novoco dev on 08/10/2020.
//

import Foundation

public enum Multibase: Character {

    case Identity           = "\u{00}"
    case Base2              = "0"
    case Base8              = "7"
    case Base10             = "9"
    case Base16             = "f"
    case Base16Upper        = "F"
    case Base32             = "b"
    case Base32Upper        = "B"
    case Base32pad          = "c"
    case Base32padUpper     = "C"
    case Base32hex          = "v"
    case Base32hexUpper     = "V"
    case Base32hexPad       = "t"
    case Base32hexPadUpper  = "T"
    case Base36             = "k"
    case Base36Upper        = "K"
    case Base58BTC          = "z"
    case Base58Flickr       = "Z"
    case Base64             = "m"
    case Base64url          = "u"
    case Base64pad          = "M"
    case Base64urlPad       = "U"
}
